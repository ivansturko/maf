import math

"HST Modelling Code v1.3"
"Coded by David Gardiner"
"Modified by Mathew Ruda"
"Last Modified: 05/23/2019"

file= open('MAANE5.dat','w+')

P2=95837.13

P3=95837.13

PA=95837.13



T1=300
"Bottles"

T2=300
"Heater"

T3=300
"Test Section"

TA=300
"Ambient"

"V1 is the total volume of all the tanks"
"The old Russian setup uses V1 = 320.73e-3"
"The new setup uses V1 = 1282.91e-3"

V1=1282.91e-3

"D1 is the bottle orafice exit diameter"
"The old Russian setup uses D1 = 9.6e-3"
"The new setup uses D1 = 11.1125e-3"

D1=11.1125e-3

DQ=500000

R=287.058
"8314/29"

K=1.4
"gamma for air"

PRcrit=(2/(K+1))**(K/(K-1))
".52828, Pressure ratio to create shock -> max flow rate"

K1=math.sqrt(2*K*R/(K-1)) 

K2=2/K

K3=(K+1)/K

K4=((2/(K+1))**(1/(K-1)))*math.sqrt((K-1)/(K+1))

cp=K*R/(K-1) 



print("\n")

print('PROGRAM "MAF"\n')

print("\n")

print("This program is intended for the unsteady gas dynamic\n")

print("processes in the Model Aerodyanmic Facility simulation.\n")

print("\n")

print("The flow parameters of the perfect air inside settling\n")

print("chamber as well as the nozzle exit are calculated\n")

print("upon the given initial conditions.\n")

print("\n")

print("The next constants are used:\n")

print("\n")

print("Initial pressure in the facility contour:       1 bar\n")

print("The volume of the accumulator:                  320 dm3\n")

print("The volume of the heater:                       10 dm3\n")

print("The volume of the settling chamber:             7 dm3\n")

print("The diameter of the tube from the accumulator:  9.6 mm\n")

print("The diameter of the tube from the heater:       20 mm\n")

print("The diameter of the nozzle exit:                100 mm\n")

print("\n")



file.write("Output data file")

file.write('\n')

file.write('\n')

file.write('\n')



heater = input("Is a heater included in the facility setup (Yes/No): ")

while True:

    if heater == 'Yes':

        TH = float(input("Indicate the temperature of the heater (K): "))

        file.write('A heater with temperature of {}K was included in the setup'.format(TH) + ".\n")

        break

    elif heater == 'No':

        TH=0;

        DQ=0;

        file.write("A heater was not used in the setup.\n")

        break

    else:

        print('Please enter Yes or No')

        heater = input("Is a heater included in the facility setup (Yes/No): ")

file.write("\n")



P1 = float(input('Indicate the pressure of accumulated gas in the bottles (bar): '))

file.write("The pressure in the bottles was {} bar".format(P1)+"\n")

file.write("\n")

D4 = float(input('Indicate the nozzle exit diameter (m): '))
file.write("Nozzle exit diameter was {} meters".format(D4)+"\n")
file.write("\n")

P1=int(P1*1e5)
"convert bar to Pa"



M = float(input('Indicate flow Mach number: '))

file.write("Flow Mach number is {}".format(M) + "\n")

AM=M

LA=math.sqrt(((K+1)/2*M*M)/(1+(K-1)/2*M*M))

Q=LA*((((K+1)/2*(1-(K-1)/(K+1)*LA*LA)))**(1/(K-1)))

D3=D4*math.sqrt(Q)
"D3=0.0596"


file.write("\n")



if heater == 'Yes':

    V2 = 10e-3
    "Volume of heater"

    V3 = 7e-3
    "Volume of test section"

    "D2 = 20e-3"
    D2 = 3.556e-3
    "D2 = 4.0e-3"
    DKR = D3 

else:

    V2 = 7e-3
    "V2 = .016272"

    V3 = 7000e-3
    "Large to simulate ambient?"

    D2 = D3

    DKR = D2

    D3 = 100e-3



A1 = math.pi*(D1**2)/4
"Bottle Area"

A2 = 0.287685749*24*math.pi*(D2**2)/4
"Heater Area - Old Spritzer"

"A2 = 0.287685749*32*math.pi*(D2**2)/4"
"Heater Area - New Spritzer"

A3 = math.pi*(D3**2)/4
"Test Area"

ANE = math.pi*(D4**2)/4
"Nozzle Exit Area"

FKR = math.pi*(DKR**2)/4 



TZ = float(input('Indicate the time interval from the beginning until the' \

               ' main valve is switched off (s): '))

file.write("The time interval of the open valve is {}s".format(TZ)+"\n")

TMAX=TZ+4.0
"Time the code runs for, time valve is open +4.0s"


"""This number can be changed for different time intervals. It was originally .000005"""
DT=0.000005
"Time step"

N1=0.01

N2=0

file.write("\n")

file.write("\n")



if heater == 'Yes':

    file.write('M={}, P1={}bar, P2={}bar, P3={}bar'.format(M,P1/1e5,P2/1e5,P3/1e5)+

               ' Th={}K'.format(TH)+"\n")

    file.write('D1={}m, D2={}m, D3={}m'.format(D1,D2,"{0:.3f}".format(D3)) + "\n")

    file.write('V1={}m3, V2={}m3, V3={}m3, tz={}s'.format(V1,V2,V3,TZ)+"\n")

    file.write('\n')

    file.write('J (s)  , P1 (bar), P2 (bar), P3 (bar), P3 (psi), T1 (K), T2 (K), T3 (K), PS (bar),'+

               ' TS (K), RO (kg/m3), W (m/s), massflow (kg/s), massflow (lbm/s), q (bar), M'+'\n')

               

else:

    file.write('M={}, P1={}bar, P2={}bar, P3={}bar'.format(M,P1/1e5,P2/1e5,P3/1e5)+

               "\n")

    file.write('D1={}m, D2={}m, D3={}m'.format(D1,D2,"{0:.3f}".format(D3)) + "\n")

    file.write('V1={}m3, V2={}m3, V3={}m3, tz={}s'.format(V1,V2,V3,TZ)+"\n")

    file.write('\n')

    file.write('J (s)  , P1 (bar), P2 (bar), P2 (psi), T1 (K), T2 (K), PS (bar),'+

               ' TS (K), RO (kg/m3), W (m/s), massflow (kg/s), massflow (lbm/s), q (bar), M'+'\n')


"Initialize variables"
I=0

mdot1=0

mdot2=0

mdot3=0

massflow=0

Q11=0

Q21=0

Q22=0

Q32=0

Q33=0



TMAX=TMAX+DT

TMAX=TMAX/DT

TMAX=int(TMAX)



for J in range(0,TMAX):

    DP1 = K*K1*(-mdot1)
    "DP1 = (K-1)*(-mdot1*cp*T1)"

    P1 = P1+DT*DP1/V1

    T1 = T1+DT*T1*(DP1-K1*(-Q11))/P1/V1



    DP2 = K*K1*(mdot1-mdot2)+(K-1)*DQ*(TH-T2)               
    "DP2 = (K-1)*DQ*(TH-T2)+(K-1)*(mdot1*cp*T1-mdot2*cp*T2)"

    P2 = P2+DT*DP2/V2

    T2 = T2 + DT*T2*(DP2-K1*(Q21-Q22))/P2/V2   



    DP3 = K*K1*(mdot2-mdot3)               
    "DP3 = (K-1)*(mdot2*cp*T2-mdot3*cp*T3)"

    P3 = P3+DT*DP3/V3

    T3 = T3+DT*T3*(DP3-K1*(Q32-Q33))/P3/V3

    if J*DT >= N2:

        LSV=0.05639+(0.05279*M)
        "Length of supersonic part of test section"

        while True:
            
            PS = P3/((1+(K-1)*M*M/2)**(K/(K-1)))
            "Static pressure (Pa) "

            TS = T3/(1+(K-1)*M*M/2)
            "Static temperature (K)"

            RO = PS/R/TS
            "Density (kg/m3)"

            W = M*math.sqrt(K*PS/RO)
            "Airspeed (m/s)"

            MU = ((1.7894*(10**-5))*((TS/273.11)**1.5))*((273.11+110.56)/(TS+110.56))
            "Sutherlands relation for dyanmic viscosity"
                                                         
            RE = RO*W*LSV/MU
            "Reynolds number"

            DELTA = 0.032*LSV*(M**1.15)/(RE**.2)
            "size of boundary layer in test section"

            FPS = 4*ANE*DELTA/D4*(1-DELTA/D4)

            F4 = ANE-FPS

            D4 = math.sqrt(4*F4/math.pi)

            Q = FKR/F4

            LA = 1

            while True:

                "print(LA)"
                "print(J*DT)"
                LA=LA+.001

                QQ=LA*((K+1)/2*(1-(K-1)/(K+1)*LA*LA))**(1/(K-1))

                if QQ-Q < 0: 

                    break

            MM = math.sqrt(2*LA*LA/(K+1)/(1-(K-1)/(K+1)*LA*LA))
            "print(M,MM)"
            if M-MM >= .01:

                M=MM

            else:

                break

        M=MM        

        if I == 0:
            "print(math.log(PS/(1e5)),PS/1e5,(4.114-336.3/TS))"
            if math.log(PS/(1e5)) > (4.114-336.3/TS):
                
                "Check for condensation"
                
                print("Flow Condensation")

                file.write("Flow condensation at t = {}s".format(J*DT)+"\n")

                I = 1
                "If flow condenses, never check again"
            else:
                "essentially checking for flow condensation every .01 seconds"
                "If no condesation the above values for station 3 keep for .01s"
                N2 = N2 + N1



        if heater =='Yes':

           file.write('{}, {}, {}, {}, {},'.format("{0:.6f}".format(J*DT),"{0:.2f}".format(P1/1e5),"{0:.2f}".format(P2/1e5),"{0:.2f}".format(P3/1e5),"{0:.2f}".format(14.50377*P3/1e5))+

                      ' {}, {}, {}, {},'.format("{0:.1f}".format(T1),"{0:.1f}".format(T2),"{0:.1f}".format(T3),"{0:.2f}".format(PS/1e5))+

                      ' {}, {}, {},'.format("{0:.1f}".format(TS),"{0:.2f}".format(RO),"{0:.2f}".format(W))+

                      ' {}, {}, {}, {}'.format("{0:.2f}".format(massflow),"{0:.2f}".format(massflow*2.204623),"{0:.2f}".format(RO*W*W/2e5),"{0:.3f}".format(M))+'\n')

        else:

            file.write('{}, {}, {},'.format("{0:.6f}".format(J*DT),"{0:.2f}".format(P1/1e5),"{0:.2f}".format(P2/1e5),"{0:.2f}".format(14.50377*P2/1e5))+

                      ' {}, {}, {},'.format("{0:.1f}".format(T1),"{0:.1f}".format(T2),"{0:.2f}".format(PS/1e5))+

                      ' {}, {}, {},'.format("{0:.1f}".format(TS),"{0:.2f}".format(RO),"{0:.2f}".format(W))+

                      ' {}, {}, {}, {}'.format("{0:.2f}".format(massflow),"{0:.2f}".format(massflow*2.204623),"{0:.2f}".format(RO*W*W/2e5),"{0:.3f}".format(M))+'\n')                                                             

        

        

    M = AM

    if J*DT > TZ:

        A1=0   

    PR1 = P2/P1
    "Pressure ratio to determine direction of flow"

    if PR1 < 1:
        "If P2 is less than P1 (Ideal)"

        if PR1 > PRcrit:

            mdot1 = A1*P1*math.sqrt(T1*(PR1**K2-PR1**K3))
            "Choked flow rate"

        else:

            mdot1 = A1*P1*(T1**.5)*K4
            "unchoked flow rate"

        Q11 = mdot1

        Q21 = mdot1*T2/T1

    else:

        PR1 = P1/P2

        if PR1 > PRcrit:

            mdot1 = -A1*P2*math.sqrt(T2*(PR1**K2-PR1**K3))
            
        else:

            mdot1 = -A1*P2*(T2**.5)*K4

        Q11 = mdot1*T1/T2

        Q21 = mdot1

    

    PR2 = P3/P2

    if PR2 < 1:

        if PR2 > PRcrit:

            mdot2 = A2*P2*math.sqrt(T2*(PR2**K2-PR2**K3))
            "Choked flow rate"

        else:

            mdot2 = A2*P2*(T2**.5)*K4
            "unchoked flow rate"

        Q22 = mdot2

        Q32 = mdot2*T3/T2

    else:

        PR2 = P2/P3

        if PR2 > PRcrit:

            mdot2 = -A2*P3*math.sqrt(T3*(PR2**K2-PR2**K3))

        else:

            mdot2 = -A2*P3*(T3**.05)*K4

        Q22 = mdot2*T2/T3

        Q32 = mdot2

    PR3 = PA/P3

    if PR3 < 1:

        if PR3 > PRcrit:

            mdot3 = A3*P3*math.sqrt(T3*(PR3**K2-PR3**K3))

        else:

            """This mdot3 statement originally had a == which does somewhat significantly change the data output"""
            mdot3 = A3*P3*(T3**.5)*K4

        massflow = ((A3*P3)/math.sqrt(T3))*math.sqrt(K/R)*((1+((K-1)/2))**((-1*(K+1))/(2*(K-1))))
        "The mass flow is output in kg/s, all inputs taken at the throat, hence M = 1"
      
        Q33 = mdot3

    else:

        PR3 = P3/PA

        if PR3 > PRcrit:

            mdot3 = -A3*PA*math.sqrt(TA*(PR3**K2-PR3**K3))

        else:

            mdot3 = -A3*PA*(TA**.5)*K4

        massflow = ((A3*PA)/math.sqrt(TA))*math.sqrt(K/R)*((1+((K-1)/2))**((-1*(K+1))/(2*(K-1))))
        "The mass flow is output in kg/s, all inputs taken at the throat, hence M = 1"

        Q33 = mdot3*T3/TA
file.close()
